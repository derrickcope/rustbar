//
// main.rs
// Copyright (C) 2019 derrick cope <derrick(at)thecopes.me>
// Distributed under terms of the MIT license.
//

extern crate rustbar;
fn main() {
    //rustbar::content::title();
    // rustbar::content::desktop();
    ////rustbar::content::datetime();
    // rustbar::content::batter();
    //rustbar::content::memory();
    //rustbar::content::store();
    //rustbar::config::yaml_config();
    //println!("{:?}", rustbar::config::yaml_config());
    //rustbar::content::parse_state();

    loop {
        println!(
            "{} {} {}",
            rustbar::config::right(),
            rustbar::config::center(),
            rustbar::config::left()
        );
    }
}

//
// content.rs
// Copyright (C) 2019 derrick cope <derrick(at)thecopes.me>
// Distributed under terms of the MIT license.
//
use std::process::Command;

pub fn title() -> String {
    let mut command = Command::new("xtitle");

    let output = command.output().expect("failed").stdout;

    let output = String::from_utf8(output).unwrap();
    let output = output.trim();

    output.to_string()
}

pub fn desktop() -> String {
    let mut command = Command::new("bspc");
    command.arg("wm").arg("-g");

    let output = command.output().expect("failed").stdout;
    //println!("{:?}", String::from_utf8(output.stdout).unwrap());

    let output = String::from_utf8(output).unwrap();
    let output = output.trim();

    // println!("{}", parse_state(output));
    parse_state(output)
}

use crate::config;

pub fn parse_state(mut output: &str) -> String {
    let whatever = config::yaml_config();
    let desktops = whatever[0][6]["config"]["desktops"].as_i64().unwrap();
    let monitors = whatever[0][6]["config"]["monitors"].as_i64().unwrap();
    //println!("{}", desktops);
    //println!("{}", monitors);
    let y: &[_] = &['W'];
    output = output.trim_start_matches(y);
    //println!("{}", output);

    let mut output_vec: Vec<&str> = output.split(":").collect();
    output_vec.retain(|&x| !x.starts_with("L") && !x.starts_with("T") && !x.starts_with("G"));

    // println!("{:?}", output_vec);
    let (output_vec_one, output_vec_two) = output_vec.split_at(5);

    // println!("{:?}", output_vec_one);
    // println!("{:?}", output_vec_two);

    let mut output_string = String::new();

    for put in 0..output_vec_one.len() {
        let select = output_vec_one[0].to_string().remove(0);
        let first = output_vec_one[put].to_string().remove(0);
        let x: &[_] = &['o', 'O', 'f', 'F', 'u', 'U', 'm', 'M'];
        if select == 'M' && (first == 'F' || first == 'U' || first == 'O') {
            output_string += "%{R} [";
            output_string.push_str(output_vec_one[put].trim_start_matches(x));
            output_string += "] %{R} ";
        } else {
            output_string.push_str(output_vec_one[put].trim_start_matches(x));
            output_string += "  ";
        }
    }

    for put in 0..output_vec_two.len() {
        let select = output_vec_two[0].to_string().remove(0);
        let first = output_vec_two[put].to_string().remove(0);
        let x: &[_] = &['o', 'O', 'f', 'F', 'u', 'U', 'm', 'M'];
        if select == 'M' && (first == 'F' || first == 'U' || first == 'O') {
            output_string += "%{R} [";
            output_string.push_str(output_vec_two[put].trim_start_matches(x));
            output_string += "] %{R} ";
        } else {
            output_string.push_str(output_vec_two[put].trim_start_matches(x));
            output_string += "  ";
        }
    }

    output_string
}

pub fn datetime() -> String {
    let mut command = Command::new("date");

    let output = command.output().expect("failed to get date").stdout;

    let output = String::from_utf8(output).unwrap();
    let output = output.trim();

    let v: Vec<&str> = output.split_whitespace().collect();

    let mut datetime = String::new();
    datetime.push_str(v[0]);
    datetime += ". ";
    datetime.push_str(v[1]);
    datetime += " ";
    datetime.push_str(v[2]);
    datetime += ", ";
    datetime.push_str(v[5]);
    datetime += " ";
    datetime.push_str(v[3]);

    //println!("{}", datetime);
    datetime
}

//pub fn batter() -> String {
//let mut command = Command::new("acpi");
//
//let output = command.output().expect("failed to get battery").stdout;
//let output = String::from_utf8(output).unwrap();
//let output = output.trim();

//println!("{}", output);

//let v: Vec<&str> = output.split_whitespace().collect();

//let mut batter = String::new();

//println!("{:?}", v);
//batter.push_str(v[2]);
//batter.pop();
//batter += ": ";
//batter.push_str(v[3]);
//batter.pop();

//batter
//}

pub fn memory() -> String {
    let mut command = Command::new("free");
    command.arg("-hg");

    let output = command.output().expect("failed to get free").stdout;
    let output = String::from_utf8(output).unwrap();
    let output = output.trim();

    //println!("{}", output);

    let v: Vec<&str> = output.split_whitespace().collect();
    //println!("{} {}/{}", v[6], v[8], v[7]);

    let mut memory = String::new();
    memory.push_str(v[6]);
    memory += " ";
    memory.push_str(v[8]);
    memory += "/";
    memory.push_str(v[7]);

    //println!("{}", memory);
    memory
}

pub fn store() -> String {
    let mut command = Command::new("df");
    command.arg("-h").arg("/dev/nvme0n1p1");

    let output = command.output().expect("failed to get store").stdout;
    let output = String::from_utf8(output).unwrap();
    let output = output.trim();

    //println!("{}", output);

    let v: Vec<&str> = output.split_whitespace().collect();
    //println!("{:?}", v);

    let mut storage = String::new();
    storage.push_str(v[12]);
    storage += ": ";
    storage.push_str(v[9]);
    storage += "/";
    storage.push_str(v[8]);
    storage += " ";
    storage.push_str(v[11]);

    //println!("{}", storage);

    storage
}

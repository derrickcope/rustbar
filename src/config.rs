//
// config.rs
// Copyright (C) 2019 derrick cope <derrick(at)thecopes.me>
// Distributed under terms of the MIT license.
//

extern crate yaml_rust;

use std::fs::File;
use std::io::prelude::*;
use yaml_rust::{Yaml, YamlLoader};

pub fn yaml_config() -> Vec<Yaml> {
    let yaml = "/home/derrick/.config/rustbar/config.yml";

    let mut handle = File::open(yaml).expect("not found");

    let mut config = String::new();

    handle.read_to_string(&mut config).expect("unable to read");

    let docs = YamlLoader::load_from_str(config.as_str()).unwrap();

    docs
}

use crate::content;

pub fn right() -> String {
    let whatever = yaml_config();
    let mut right_arr: Vec<String> = vec!["%{r}".to_string()];
    let content_arr = vec![
        content::desktop(),
        content::title(),
        //content::batter(),
        content::memory(),
        content::store(),
        content::datetime(),
    ];
    //println!("{:?}", content_arr);

    for value in 0..5 {
        if whatever[0][value]["block"]["alignment"].as_str().unwrap() == "r"
            && whatever[0][value]["block"]["display"].as_str().unwrap() == "yes"
        {
            right_arr.push((&content_arr[value]).to_string());
            right_arr.push("| ".to_string());
        }
    }
    let right = right_arr.join(" ");
    //println!("{:?}", right);
    right
}

pub fn center() -> String {
    let whatever = yaml_config();
    let mut center_arr: Vec<String> = vec!["%{c}".to_string()];
    let content_arr = vec![
        content::desktop(),
        content::title(),
        //content::batter(),
        content::memory(),
        content::store(),
        content::datetime(),
    ];
    //println!("{:?}", content_arr);

    for value in 0..5 {
        if whatever[0][value]["block"]["alignment"].as_str().unwrap() == "c"
            && whatever[0][value]["block"]["display"].as_str().unwrap() == "yes"
        {
            center_arr.push((&content_arr[value]).to_string());
            //right_arr.push("|".to_string());
        }
    }
    let center = center_arr.join(" ");
    //println!("{:?}", right);
    center
}

pub fn left() -> String {
    let whatever = yaml_config();
    let mut left_arr: Vec<String> = vec!["%{l}".to_string()];
    let content_arr = vec![
        content::desktop(),
        content::title(),
        //content::batter(),
        content::memory(),
        content::store(),
        content::datetime(),
    ];
    //println!("{:?}", content_arr);

    for value in 0..5 {
        if whatever[0][value]["block"]["alignment"].as_str().unwrap() == "l"
            && whatever[0][value]["block"]["display"].as_str().unwrap() == "yes"
        {
            left_arr.push("| ".to_string());
            left_arr.push((&content_arr[value]).to_string());
        }
    }
    let left = left_arr.join(" ");
    //println!("{:?}", right);
    left
}
